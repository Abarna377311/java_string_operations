
public class Ass4 {

	/*Create a JAVA program to split the strings based on white-spaces, 
	 * after the split operation, print all the strings in the dictionary order. */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s="Welcome to Java Programming";
		String tmp="";
		String[] split_arr=s.toUpperCase().split(" ");
		System.out.println("Original String order :");
		for(int i=0;i<split_arr.length;i++)
		{
			System.out.println("split ["+i+"]"+split_arr[i]);
		}
		System.out.println("The given String in Dictionary order :");
		for(int i=0;i<split_arr.length;i++)
		{
			for(int j=i+1;j<split_arr.length;j++)
			{
				if(split_arr[i].charAt(0)>split_arr[j].charAt(0))
				{
					tmp=split_arr[i];
					split_arr[i]=split_arr[j];
					split_arr[j]=tmp;
				}
			}
			System.out.println("Split ["+i+"]"+split_arr[i]);
		}
	}
}
