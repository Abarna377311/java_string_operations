
public class Ass1 {
	
	/*Write a JAVA program to reverse the given String S. 
	 * Perform the reversal operation if and only if the string S is equivalent to S1 are equal. 
	 *  (Perform case-sensitive comparison). If the strings are not equal print �Reverse Not Supported�.
	 *  Example: String S = �Hari�, S1 = �Wipro�, output should be �Reverse Not Supported�. */
	
public static void main(String args[])
{
	String S="Hari",S1="Wipro",res="";
	char c[]=new char[S.length()];
	if(S.equals(S1))
	{
		for(int i=S.length()-1;i>=0;i--)
		{
			c[i]=S.charAt(i);
			res+=Character.toString(c[i]);
		}
		System.out.println("Character Array: "+res);
	}
	else
		System.out.println("Revere Not Supported");
}
}
