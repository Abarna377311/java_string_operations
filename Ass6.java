
public class Ass6 {

	/*Create a JAVA program to rotate the string by clock-wise direction 
	 * if the length is even and to rotate the string to the anti-clockwise direction 
	 * if the length is odd. Perform one rotation at any case*/
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s="welcome",sub="",new_str="";
		int length=s.length();
		if(length%2==0)
		{
			new_str=s;
			System.out.println("Clockwise rotation of given string:"+new_str);
			for(int i=0;i<length;i++)
			{
			char c=s.charAt(i);
			sub=new_str.substring(1, length);
			new_str=sub+c;
			System.out.println(new_str);
			}
		}
		else
		{
			new_str=s;
			System.out.println("Anti-clockwise rotation of given string :"+new_str);
			for(int i=length-1;i>=0;i--)
			{
				char c=s.charAt(i);
				sub=new_str.substring(0, length-1);
				new_str=c+sub;
				System.out.println(new_str);
			}
		}
	}

}
