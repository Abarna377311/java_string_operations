
public class Ass2 {
	
	/*Write a JAVA program to count the total upper-case and lower-case 
	 * characters of a String T. After counting, print them if and only if the count is not equal. 
	 * If the count is equal, print �Equally Distributed�. */
	
public static void main(String args[])
{
	String s1="AsSiGnMeNt";
	int uppercount=0;
	String upper=s1.toUpperCase();
	for(int i=0;i<s1.length();i++)
	{
		if(s1.charAt(i)==upper.charAt(i))
		{
			uppercount++;
		}
	}
	int lowercount=s1.length()-uppercount;
	if(uppercount!=lowercount)
	{
	System.out.println("Uppercase letters count of "+s1+":"+uppercount+"\n"+"Lowercase letters count of "+s1+":"+lowercount);
	}
	else
		System.out.println("Equally Distributed");
}
}
