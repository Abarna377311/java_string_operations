
public class Ass7 {

	/*Create a JAVA program to merge two given strings S1 and S2. 
	 * Perform merge operation by taking single character from each string.
	 * Example: If the string S1 = �ABC�, S2= �DEF�, Output: ADBECF.*/
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1="ABC",s2="DEF",res="";
		System.out.println("Original String :"+s1.concat(s2));
		for(int i=0;i<((s1.length()+s2.length())/2);i++)
		{
			res+=s1.charAt(i);
			res+=s2.charAt(i);
		}
		System.out.println("The merged string is :"+res);
	}

}
