
public class Ass9 {

	/*Create a JAVA program to sort the characters of the string S. 
	 * After sorting, extract the odd-positioned characters and store it into a string variable and print it*/
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str="Assignment",new_str="";
		char c[]=str.toUpperCase().toCharArray();
		char tmp;
		for(int i=0;i<str.length();i++)
		{
			for(int j=i+1;j<str.length();j++)
			{
				if(c[i]>c[j])
				{
					tmp=c[i];
					c[i]=c[j];
					c[j]=tmp;
				}
			}
		}
		String sort_str=new String(c);
		System.out.println("Sorted string :"+sort_str);
		for(int i=0;i<sort_str.length();i++)
		{
			if(i%2!=0)
			{
				new_str+=sort_str.charAt(i);
			}
		}
		System.out.println("Odd positioned characters of the string :"+new_str);
	}

}
