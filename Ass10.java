
public class Ass10 {

	/*Write a JAVA program to compute the string age. Age of the string can be calculated using the formula: 
	 * Age = Length of the string S + Alphabetical index of the last-character. 
	 * Example: If the string is �ABAB�, output = (4+2) = 6.*/
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str="wipro";
		int index,age;
		index=str.charAt(str.length()-1)-'a'+1;
		System.out.println("Alphabetical index of the last character "+str.charAt(str.length()-1)+" :"+index);
		System.out.println("Length of the given string "+str+" :"+str.length());
		age=str.length()+index;
		System.out.println("Age of the given string '"+str+"' :"+age);	
	}

}
